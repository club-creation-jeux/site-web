---
title: Organigramme
layout: page
permalink: organigramme.html
---

# 2019 - 2020

## Bureau restreint

  * **Présidence :** Julien
  * **Vice-présidence :** Paul
  * **Trésorier :** Raphaël
  * **Secrétaire :** Alexandre Ottman

## Autres postes

  * **Respos princesse :** Julien, Alexandre Ottman
  * **Respo comm' :** Bruno
  * **Reso Discord :** Julien
  * **Respo web :** Alexandre Conte
  * **Respo Quentin :**


# 2018 - 2019

## Bureau restreint

  * **Président :** Toni
  * **Vice-président relations extérieures :** Qàsim
  * **Vice-président relations internes :** Raphaël
  * **Trésorier :** Fabien
  * **Secrétaire :** Cyprien L

## Autres postes

  * **Princesse :** Adrien
  * **Respo carton :** Toni
  * **Respos comm' :** Fabien, Toni
  * **Code et programmation :** Qàsim, Raphaël, Cyprien E, Cyprien L, Adrien
  * **Pôle JDR :** Cyprien E
  * **Respos graphismes :** Manu, Toni
  * **Respo web :** <span style="color: #000000">M</span><span style="color: #784F17">a</span><span style="color: #E40303">t</span><span style="color: #FF8C00">t</span><span style="color: #FFED00">h</span><span color="color: #004DFF">i</span><span style="color: #004DFF">a</span><span style="color: #750787">s</span>
  * **Respo Discord :** Raphaël
  * **Pôle Formation de Game Design :** Toni, Raphaël, Qàsim, Adrien

