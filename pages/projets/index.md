---
title: Projets
layout: page
permalink: /projets/
---

## Jeux vidéos

  * 007 avec *mechas* (en dévelopement)
  * Jeu de gestion, inspiré par Papers Please, vue dans la peau de Satan
  * Projet de coop asymétrique (idée)
  * Jeux vidéo à la seconde personne (idée)

## Jeux de société

  * [Oui, Chef de projet](oui-seigneur-des-tenebres-int) (testé, prototype réalisé)
  * L'impasse mexicaine (prototype réalisé)
  * À voter (à tester, modifications si nécessaires)
  * *Kingdom Rush*, version plateau (idée)

## Autres projets

  * *Escape Game* de Novembre, pour MiNET
  * Jeu de piste géant
  * *Heroes of Might and Magic* JDR (à voir avec le respo JDR)

## GATE

[Learn By Play](/learn-by-play/) : Apprendre à des élèves de collège à créer des jeux et assimiler des notions scolaires par le jeu
