#! /bin/bash

##
#
# Not-functional for now
#
# If there have been modifications to the local master branch, it won't work as 
# planned
#
# Also, we must add a cron task to schedule the script to be run once every hour/day
#
##


CELL_WEB_DIR='~/site-web/'

cd $CELL_WEB_DIR
git checkout master
git pull origin master
bundle exec jekyll build
cp $CELL_WEB_DIR/_site /var/www/public_html/