---
title: Tuiles
date: 2018-09-27
---

Méchant : Hill, vivant sur une île

Tu heal, tue Hill, avec des tuiles sur son île !

## But

## Règles

  * Coop :
    * Récupérer des items/tuiles ?
    * Ne pas être tous au même endroit
    * Dans les combats, avoir des bonus suivant le nombre de joueurs
  * Limitation d'utilisation des tuiles
  * Utiliser des tuiles de différentes couleurs
    * Toutes les tuiles doivent pouvoir servir à "utiliser le plateau" **et** en combat
    * Bleu : cases spécifiques (marchands...)
    * Carrefour
    * Vert : Chemin facile et vie !
    * Rouge : High risk, high reward, permet de taper les mobs !
      * Trous
      * Trappes
      * Mines
      * Monstres
  * Koulicé c rigolo
  * Réserve commune ou échangeables ?
  * Pioche ? Une par couleur ?
  * Tour par tour
  * Link de tuiles : on utilise en combat les tuiles qu'on a posé pour se déplacer

## Références

  * **Room 25** :
    * Tuiles à retourner
    * Code
  * **Munchkin**
  * **Battle Royale** : Terrains se réduit
